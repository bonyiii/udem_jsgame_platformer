import Phaser from 'phaser';
import ENEMY_TYPES from '../kinds/index';
import collidable from '../mixins/collidable';

function Enemies(scene) {
  Phaser.GameObjects.Group.call(this, scene)
}
Enemies.prototype = Object.create(Phaser.GameObjects.Group.prototype)
Enemies.prototype.constructor = Enemies

Object.assign(Enemies.prototype, collidable)

Enemies.prototype.getProjectiles = function() {
  const projectiles = new Phaser.GameObjects.Group()

  this.getChildren().forEach((enemy) => {
    enemy.projectiles && projectiles.addMultiple(enemy.projectiles.getChildren())
  })

  return projectiles
}

Enemies.prototype.getTypes = function() {
  return ENEMY_TYPES
}

export default Enemies
