"use strict"

import Phaser from 'phaser';
import Collectable from '../collectables/collectable';

function Collectables(scene) {
  Phaser.Physics.Arcade.StaticGroup.call(this, scene.physics.world, scene)

  this.createFromConfig({
    classType: Collectable
  })
}
Collectables.prototype = Phaser.Physics.Arcade.StaticGroup.prototype
Collectables.prototype.constructor = Collectables

Collectables.prototype.addFromLayer = function(layer) {
  const { score: defaultScore, type } = mapProperties(layer.properties)

  layer.objects.forEach(collectableProps => {
    const collectable = this.get(collectableProps.x, collectableProps.y, type)
    const props = mapProperties(collectableProps.properties)

    collectable.score = props.score || defaultScore
  })
  //console.log("diamond scores", this.getChildren().map(c => c.score))
}

function mapProperties(propertiesList) {
  if (propertiesList && propertiesList.length > 0) {
    return propertiesList.reduce(function(map, property) {
      map[property.name] = property.value
      return map
    }, {})
  } else {
    return {}
  }
}

export default Collectables
