import * as Phaser from "phaser"
import EffectManager from "../effects/effect_manager";
import { getTimestamp } from "../utils/functions";

function MeeleWeapon(scene, x, y, key) {
  Phaser.Physics.Arcade.Sprite.call(this, scene, x, y, key)

  scene.add.existing(this)
  scene.physics.add.existing(this)

  activate(this, false)

  this.animationName = `${key}-swing`;
  this.damage = 15;
  this.attackSpeed = 1000;
  this.timeFromLastSwing = null;
  this.effectManager = new EffectManager(scene)

  this.setOrigin(0.5, 1)
  this.setDepth(10)

  this.on('animationcomplete', destroy, this)
}
MeeleWeapon.prototype = Object.create(Phaser.Physics.Arcade.Sprite.prototype)
MeeleWeapon.prototype.constructor = MeeleWeapon

MeeleWeapon.prototype.deliversHit = function(target) {
  const impactPosition = { x: this.x, y: this.getRightCenter().y }
  this.effectManager.playEffectOn(target, 'hit-effect', impactPosition)
  this.body.checkCollision.none = true
}

MeeleWeapon.prototype.swing = function(wielder, soundEffect) {
  // Wait till animation is finished
  if (this.active) { return }
  if (this.timeFromLastSwing &&
    this.timeFromLastSwing + this.attackSpeed > getTimestamp()) { return }

  this.wielder = wielder
  activate(this, true)
  soundEffect.play()
  wielder.play('throw', true)
  this.anims.play(this.animationName)
  this.timeFromLastSwing = getTimestamp()
}

MeeleWeapon.prototype.preUpdate = function(time, delta) {
  MeeleWeapon.prototype.__proto__.preUpdate.call(this, time, delta)

  if (!this.active) { return }

  if (this.wielder.lastDirection === Phaser.Physics.Arcade.FACING_RIGHT) {
    this.setFlipX(false)
    this.body.reset(this.wielder.x + 15, this.wielder.y)
  } else {
    this.setFlipX(true)
    this.body.reset(this.wielder.x - 15, this.wielder.y)
  }
}

function activate(self, isActive) {
  self.setActive(isActive);
  self.setVisible(isActive);
}

function destroy(animation) {
  if (animation.key === this.animationName) {
    activate(this, false)
    this.body.reset(0, 0)
    this.body.checkCollision.none = false
  }
}

export default MeeleWeapon
