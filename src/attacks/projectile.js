"use strict"

import Phaser from 'phaser'
import EffectManager from '../effects/effect_manager';

function Projectile(scene, x, y, key) {
  Phaser.Physics.Arcade.Sprite.call(this, scene, x, y, key)

  scene.add.existing(this);
  scene.physics.add.existing(this);
  const effectManager = new EffectManager(scene)

  this.speed = 300;
  this.maxDistance = 200;
  this.travelDistance = 0;
  this.cooldown = 500;
  this.damage = 10;
  this.body.setSize(this.width - 13, this.height - 20)

  this.deliversHit = function(target) {
    const impactPosition = { x: this.x, y: this.y }
    // in this case "this" is bound to the object created by the
    //  constructor function Projectile and point to that object
    reset(this)
    // effectManager kept and avaialble becuase of static scoping
    // so even the returned object is not in the scope of the
    // Projectile constructor function after it returned it keeps
    // link to the environment in which it was created and access
    // effectManager becuase of that
    effectManager.playEffectOn(target, 'hit-effect', impactPosition)
  }
}
Projectile.prototype = Object.create(Phaser.Physics.Arcade.Sprite.prototype)

Projectile.prototype.preUpdate = function(time, delta) {
  Projectile.prototype.__proto__.preUpdate.call(this, time, delta)

  this.travelDistance += this.body.deltaAbsX();

  if (isOutOfRange(this)) { reset(this) }
}

Projectile.prototype.fire = function(x, y, anim) {
  activate(this, true);
  this.body.reset(x, y);
  this.setVelocityX(this.speed)
  anim && this.play(anim, true)
}

function isOutOfRange(self) {
  return self.travelDistance >= self.maxDistance
}

function reset(self) {
  activate(self, false);
  self.body.reset(0, 0)
  self.travelDistance = 0;
}

function activate(self, isActive) {
  self.setActive(isActive);
  self.setVisible(isActive);
}

export default Projectile;
