import Phaser from 'phaser';
import { getTimestamp } from '../utils/functions';
import Projectile from "./projectile";

function Projectiles(scene, key) {
  Phaser.Physics.Arcade.Group.call(this, scene.physics.world, scene);

  this.timeFromLastProjectile = null;

  this.createMultiple({
    frameQuantity: 5,
    active: false,
    visible: false,
    key: key,
    classType: Projectile
  })
}
Projectiles.prototype = Object.create(Phaser.Physics.Arcade.Group.prototype)
Projectiles.prototype.constructor = Projectiles

Projectiles.prototype.fireProjectile = function(initiator, anim) {
  const projectile = this.getFirstDead(false);

  if (!projectile) { return }
  if (this.timeFromLastProjectile &&
    this.timeFromLastProjectile + projectile.cooldown > getTimestamp()) { return }

  const center = initiator.getCenter();
  let centerX;

  if (initiator.lastDirection === Phaser.Physics.Arcade.FACING_RIGHT) {
    projectile.speed = Math.abs(projectile.speed)
    projectile.setFlipX(false)
    centerX = center.x + 10
  } else {
    projectile.speed = -Math.abs(projectile.speed)
    projectile.setFlipX(true)
    centerX = center.x - 10
  }

  projectile.fire(centerX, center.y, anim);
  this.timeFromLastProjectile = getTimestamp();
}

export default Projectiles;
