import Phaser from "phaser";
import SHARED_CONFIG from "./config.js";
import CreditScene from "./scenes/credits.js";
import LevelScene from "./scenes/level.js";
import MenuScene from "./scenes/menu.js";
import PlayScene from './scenes/play.js'
import PreloadScene from './scenes/preload.js'

const scenes = [PreloadScene, PlayScene, MenuScene, LevelScene, CreditScene];
const createScene = (Scene) => new Scene()
const initScenes = () => scenes.map(createScene)

const config = {
  type: Phaser.AUTO,
  ...SHARED_CONFIG,
  pixelArt: true,
  physics: {
    default: 'arcade',
    arcade: {
      debug: SHARED_CONFIG.debug
    }
  },
  scene: initScenes()
};

new Phaser.Game(config);

