import * as Phaser from "phaser";

export default {
  addCollider(otherGameObjects, callback, context) {
    this.scene.physics.add.collider(this, otherGameObjects, callback, null, context || this)
    return this;
  },

  addOverlap(otherGameObjects, callback, context) {
    this.scene.physics.add.overlap(this, otherGameObjects, callback, null, context || this)
    return this;
  },

  bodyPositionDifferenceX: 0,
  prevRay: null,
  prevHasHit: null,

  raycast(body, { raylength = 30, precision = 0, steepnes = 1 }) {
    this.bodyPositionDifferenceX += body.x - body.prev.x;
    if (this.prevHasHit && Math.abs(this.bodyPositionDifferenceX) <= precision) {
      return {
        ray: this.prevRay,
        hasHit: this.prevHasHit
      }
    }

    const { x, y, width, halfHeight } = body
    const line = new Phaser.Geom.Line()
    let hasHit = false

    switch (body.facing) {
      case Phaser.Physics.Arcade.FACING_RIGHT: {
        line.x1 = x + width
        line.y1 = y + halfHeight
        line.x2 = line.x1 + raylength * steepnes
        line.y2 = line.y1 + raylength
        break
      }
      case Phaser.Physics.Arcade.FACING_LEFT: {
        line.x1 = x
        line.y1 = y + halfHeight
        line.x2 = line.x1 - raylength * steepnes
        line.y2 = line.y1 + raylength
        break
      }
    }

    const hits = this.platformsColliders.getTilesWithinShape(line)

    if (hits.length > 0) {
      hasHit = this.prevHasHit = hits.some(tile => tile.index > -1)
    }

    this.prevRay = line
    this.bodyPositionDifferenceX = 0

    return { ray: line, hasHit }
  }
}
