export default {
  // because in Object.assign we assign this function to self
  // self here is this. That's why this is used instead of self.
  isPlayingAnims(animsKey) {
    return this.anims.isPlaying && this.anims.getCurrentKey() === animsKey
  }
}
