"use strict"

import Phaser from 'phaser'
import SHARED_CONFIG from '../config'

const fontSize = 20;

// Heads up display
function Hud(scene, x, y) {
  Phaser.GameObjects.Container.call(this, scene, x, y)

  scene.add.existing(this)

  const { rightTopCorner } = SHARED_CONFIG
  this.setPosition(rightTopCorner.x - 75, rightTopCorner.y + 5)
  this.setScrollFactor(0)

  setupList(this)
}
Hud.prototype = Object.create(Phaser.GameObjects.Container.prototype)
Hud.prototype.constructor = Hud

Hud.prototype.updateScoreBoard = function(score) {
  // destructure array by their index an locally assign name to them
  // since in createScoreboard function items [text, image] added
  // to the array we can name them here as we want, though order matter
  const [scoreText, scoreImage] = this.getByName("scoreBoard").list
  scoreText.setText(score)
  scoreImage.setX(scoreText.width + 5)
}

function setupList(hud) {
  const scoreBoard = createScoreBoard(hud)

  hud.add([scoreBoard])

  let lineHeight = 0;
  hud.list.forEach(item => {
    item.setPosition(item.x, item.y + lineHeight)
    lineHeight += 20
  })
}

function createScoreBoard(hud) {
  const text = hud.scene.add.text(0, 0, '0', { fontSize: `${fontSize}px`, fill: "#fff" })
  const image = hud.scene.add.image(text.width + 5, 2, 'diamond')
    .setOrigin(0, 0)
    .setScale(1.3)

  const scoreBoard = hud.scene.add.container(0, 0, [text, image])
  scoreBoard.setName("scoreBoard")

  return scoreBoard
}

export default Hud
