"use strict"

import Phaser from 'phaser'

function HealthBar(scene, x, y, scale, initialHealth) {
  const self = this

  x = x / scale;
  y = y / scale;

  const size = {
    width: 40,
    height: 8
  }
  const pixelPerHealth = size.width / initialHealth

  this.value = initialHealth;

  const bar = new Phaser.GameObjects.Graphics(scene);
  scene.add.existing(bar)
  bar.setScrollFactor(0, 0).setScale(scale)

  this.decrease = function(amount) {
    this.value = (amount > 0) ? amount : 0;
    draw()
  }

  draw()

  function draw() {
    bar.clear()
    const margin = 2;

    bar.fillStyle(0x000000)
    bar.fillRect(x, y, size.width + margin, size.height + margin)

    bar.fillStyle(0xFFFFFF)
    bar.fillRect(x + margin, y + margin, size.width - margin, size.height - margin)

    const healthWidth = Math.floor(self.value * pixelPerHealth);

    if (healthWidth < size.width / 3) {
      bar.fillStyle(0xFF0000)
    } else {
      bar.fillStyle(0x00FF00)
    }

    if (healthWidth > 0) {
      bar.fillRect(x + margin, y + margin, healthWidth - margin, size.height - margin)
    }
  }
}

export default HealthBar
