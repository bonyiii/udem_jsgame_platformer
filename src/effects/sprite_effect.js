import Phaser from 'phaser'

// effectName will create a function local variable
// each invocation of the function will create a separate
// self object which will keep a reference to its local
// scope eg: the function body and the local variables
// within that body. So if Spriteeffect called once with 'hit-effect'
// and another time with 'melee-effect', then the first time the
// returned self will see and keep a reference to a local scope with
// 'hit-effect' and the another invocation will see
// 'melee-effect'. So, no need to attach effectName to the self object
// unless we need to mutate directly it through the object. This is not
// the case, the effect on the instances won't change.
// eg: closures -> http://www.crockford.com/javascript/private.html
//                 https://www.tutorialsteacher.com/javascript/closure-in-javascript
//                 http://crockford.com/javascript/inheritance.html
//                 https://www.tutorialsteacher.com/javascript/inheritance-in-javascript
//
// It is still possible to update these quazi private variables through
// accessor functions which are attached to the self, object.
// Create a function which mutate the current scope effectName like so:
// self.updateEffect = function(newEffectName)) { effectName = newEffectName }
// or in a private function which might be invoked from another publicly available
// eg. self.function = whatever(name) { updateEffect(name) }
// function updateEffect(newEffectName)) { effectName = newEffectName }
function SpriteEffect(scene, x, y, effectName, impactPosition) {
  Phaser.Physics.Arcade.Sprite.call(this, scene, x, y)

  scene.add.existing(this)
  scene.physics.add.existing(this)

  this.target = null
  this.effectName = effectName
  this.impactPosition = impactPosition

  this.on('animationcomplete', destroy, this)
}
SpriteEffect.prototype = Object.create(Phaser.Physics.Arcade.Sprite.prototype)
SpriteEffect.prototype.constructor = SpriteEffect

SpriteEffect.prototype.playOn = function(target) {
  this.target = target
  this.play(this.effectName, true)
  placeEffect(this)
}

SpriteEffect.prototype.preUpdate = function(time, delta) {
  SpriteEffect.prototype.__proto__.preUpdate.call(this, time, delta)
  placeEffect(this)
}

// Kinda private functions cannot be called on the object nor outside of this file
function placeEffect(self) {
  if (self.target && self.body) {
    const center = self.target.getCenter();
    self.body.reset(center.x, self.impactPosition.y)
  }
}

function destroy(animation) {
  if (animation.key === this.effectName) {
    this.destroy()
  }
}

export default SpriteEffect
