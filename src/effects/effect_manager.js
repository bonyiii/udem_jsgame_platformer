import SpriteEffect from "./sprite_effect"

function EffectManager(scene) {
  this.playEffectOn = function(target, effectName, impactPosition) {
    const effect = new SpriteEffect(scene, 0, 0, effectName, impactPosition)
    effect.playOn(target)
  }
}

export default EffectManager
