"use strict"

import Phaser from 'phaser';

function Collectable(scene, x, y, key) {
  Phaser.Physics.Arcade.Sprite.call(this, scene, x, y, key)

  scene.add.existing(this)
  this.score = 1;

  this.setOrigin(0, 1)

  //     y: this.y - Phaser.Math.Between(-55, 85),
  scene.tweens.add({
    targets: this,
    y: this.y - 3,
    duration: Phaser.Math.Between(1500, 2500),
    repeat: -1,
    easy: 'linear',
    yoyo: true
  })
}
Collectable.prototype = Object.create(Phaser.Physics.Arcade.Sprite.prototype)
Collectable.prototype.constructor = Collectable

export default Collectable
