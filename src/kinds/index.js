import Birdman from '../entities/birdman'
import Snaky from '../entities/snaky'

const ENEMEY_TYPES = {
  Birdman,
  Snaky
}

export default ENEMEY_TYPES
