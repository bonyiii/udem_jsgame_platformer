"use strict"

import Phaser from 'phaser'
import SHARED_CONFIG from '../config'

function BaseScene(key, config) {
  Phaser.Scene.call(this, key)
  this.screenCenter = [SHARED_CONFIG.width / 2, SHARED_CONFIG.height / 2]
  this.fontSize = 75
  this.lineHeight = 82
  this.fontOptions = { fontSize: `${this.fontSize}px`, fill: "#713E01" }
  this.config = config
}
BaseScene.prototype = Object.create(Phaser.Scene.prototype)
BaseScene.prototype.constructor = BaseScene

BaseScene.prototype.create = function() {
  this.add.image(0, 0, 'menu-bg').setOrigin(0, 0).setScale(2.7)

  if (this.config.canGoBack) {
    const backButton = this
      .add.image(SHARED_CONFIG.width - 10, SHARED_CONFIG.height - 10, 'back')
      .setOrigin(1).setScale(2).setInteractive();
    backButton.on('pointerup', () => this.scene.start('MenuScene'));
  }
}

BaseScene.prototype.createMenu = function(setupMenuEvents) {
  let lastMenuPosition = 0
  this.menu.forEach(menuItem => {
    const menuPosition = [this.screenCenter[0], this.screenCenter[1] + lastMenuPosition];
    menuItem.textGO = this.add.text(...menuPosition, menuItem.text, this.fontOptions).setOrigin(0.5, 1);
    lastMenuPosition += this.lineHeight;
    setupMenuEvents(this, menuItem)
  })
}

export default BaseScene
