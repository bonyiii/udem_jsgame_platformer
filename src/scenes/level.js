"use strict"

import Phaser from 'phaser'
import BaseScene from './base_scene'

function LevelScene() {
  BaseScene.call(this, this.constructor.name, { canGoBack: true })
}
LevelScene.prototype = Object.create(BaseScene.prototype)
LevelScene.prototype.constructor = LevelScene

LevelScene.prototype.create = function() {
  // call super, eg: Basescene.prototype
  LevelScene.prototype.__proto__.create.call(this)

  this.menu = []
  const levels = this.registry.get('unlocked-levels')

  for (let i = 1; i <= levels; i++) {
    this.menu.push({
      scene: 'PlayScene', text: `Level ${i}`, level: i
    })
  }
  this.createMenu(setupEvents)
}

function setupEvents(menuScene, menuItem) {
  const textGO = menuItem.textGO;
  textGO.setInteractive()

  textGO.on('pointerover', () => {
    textGO.setStyle({ fill: "#ff0" })
  })

  textGO.on('pointerout', () => {
    textGO.setStyle({ fill: "#713E01" })
  })

  textGO.on('pointerup', () => {
    if (menuItem.scene) {
      menuScene.registry.set('level', menuItem.level)
      menuScene.scene.start(menuItem.scene)
    }

    if (menuItem.text == "Exit") {
      menuScene.game.destroy(true)
    }
  })
}

export default LevelScene
