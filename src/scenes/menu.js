"use strict"

import Phaser from 'phaser'
import BaseScene from './base_scene'

function MenuScene() {
  BaseScene.call(this, this.constructor.name, {})

  this.menu = [
    { scene: 'PlayScene', text: 'Play' },
    { scene: 'LevelScene', text: 'Levels' },
    { scene: 'CreditScene', text: 'Credits' },
    { scene: null, text: 'Exit' },
  ]
}
MenuScene.prototype = Object.create(BaseScene.prototype)
MenuScene.prototype.constructor = MenuScene

MenuScene.prototype.create = function() {
  // call super, eg: Basescene.prototype
  MenuScene.prototype.__proto__.create.call(this)
  this.createMenu(setupEvents)
}

function setupEvents(menuScene, menuItem) {
  const textGO = menuItem.textGO;
  textGO.setInteractive()

  textGO.on('pointerover', () => {
    textGO.setStyle({ fill: "#ff0" })
  })

  textGO.on('pointerout', () => {
    textGO.setStyle({ fill: "#713E01" })
  })

  textGO.on('pointerup', () => {
    menuItem.scene && menuScene.scene.start(menuItem.scene)

    if (menuItem.text == "Exit") {
      menuScene.game.destroy(true)
    }
  })
}

export default MenuScene
