"use strict"

import Phaser from 'phaser';
import SHARED_CONFIG from '../config';
import Player from '../entities/player'
import Enemies from '../groups/enemies';
import initAnims from '../anims'
import Collectables from '../groups/collectables';
import Hud from '../hud/index';
import eventEmitter, { PLAYER_LOOSE } from '../events/Emitter';

// https://javascript.info/native-prototypes#other-built-in-prototypes
// https://medium.com/@apalshah/javascript-class-difference-between-es5-and-es6-classes-a37b6c90c7f8
// maybe the best, maybe not
// https://medium.com/@lancelyao/object-create-vs-new-in-javascript-8315327bdc5a
// https://www.dofactory.com/javascript/design-patterns/factory-method

// Inharitance types explained
// https://medium.com/@PitaJ/javascript-inheritance-patterns-179d8f6c143c

function PlayScene() {
  // Dynamicly define scene name based on the constructor function
  // (eg: PlayScene) name
  Phaser.Scene.call(this, this.constructor.name)

  this.score = 0;
}
PlayScene.prototype = Object.create(Phaser.Scene.prototype)
PlayScene.prototype.constructor = PlayScene

PlayScene.prototype.create = function({ gameStatus }) {
  initAnims(this.anims)

  this.hud = new Hud(this, 0, 0)

  const map = this.make.tilemap({ key: `level_${getCurrentLevel(this)}` });
  map.addTilesetImage('main_lev_build_1', 'tiles-1');
  map.addTilesetImage('main_lev_build_2', 'tiles-2');
  map.addTilesetImage('bg_spikes_tileset', 'bg-spikes-tileset');
  //const tileset2 = map.addTilesetImage('main_lev_build_2', 'tiles-2');

  const layers = createLayers(this, map);
  const playerZones = getZones(layers.playerZones.objects);
  const player = createPlayer(this, playerZones.start);
  const enemies = createEnemies(this, layers.enemySpawns, layers.colliders);
  const collectables = createCollectables(this, layers.collectables);

  this.collectSound = this.sound.add('coin-pickup', { volume: 0.2 })

  createBg(this, map)
  playBgMusic(this)

  addPlayerColliders(this, player, {
    colliders: {
      platformsColliders: layers.colliders,
      projectiles: enemies.getProjectiles(),
      collectables: collectables,
      traps: layers.traps
    }
  })

  addEnemyColliders(enemies, {
    colliders: {
      platformsColliders: layers.colliders,
      player
    }
  })

  createBackButton(this)
  endOfLevel(this, playerZones.end, player)
  setupCamera(this, player);

  if (gameStatus === PLAYER_LOOSE) { return }
  createGameEvents(this)
}

PlayScene.prototype.update = function() {
  this.bgSpikes.tilePositionX = this.cameras.main.scrollX * 0.3;
  this.sky.tilePositionX = this.cameras.main.scrollX * 0.1;
}

function playBgMusic(playScene) {
  if (playScene.sound.get('theme')) { return }
  playScene.sound.add('theme', { loop: true, volume: 0.2 }).play()
}

function createBackButton(playScene) {
  const backButton = playScene.add
    .image(SHARED_CONFIG.rightBottomCorner.x, SHARED_CONFIG.rightBottomCorner.y, 'back')
    .setOrigin(1)
    .setScrollFactor(0)
    .setScale(1.5)
    .setInteractive()

  backButton.on('pointerup', () => playScene.scene.start('MenuScene'))
}

function createBg(playScene, map) {
  const bg = map.getObjectLayer('distant_bg').objects[0];
  playScene.bgSpikes = playScene.add.tileSprite(bg.x, bg.y, SHARED_CONFIG.width, bg.height, 'bg-spikes-dark')
    .setOrigin(0, 1)
    .setDepth(-3)
    .setScrollFactor(0, 1)

  playScene.sky = playScene.add.tileSprite(0, 0, SHARED_CONFIG.width, 180, 'sky-play')
    .setOrigin(0, 0)
    .setDepth(-4)
    .setScale(1.1)
    .setScrollFactor(0, 1)
}

function createGameEvents(playScene) {
  eventEmitter.on(PLAYER_LOOSE, () => {
    playScene.scene.restart({ gameStatus: PLAYER_LOOSE })
  })
}

function addPlayerColliders(context, player, { colliders }) {
  // addCollider method provided by a mixin which is invoked through Object.assign
  player
    .addCollider(colliders.platformsColliders)
    .addCollider(colliders.projectiles, onHit)
    .addOverlap(colliders.collectables, onCollect, context)
    .addCollider(colliders.traps, onHit)
}

// Called from collidable.js#addOverlap and "this" is bound to PlayScene as context
// that's why score is accessible here.
function onCollect(entity, collectable) {
  this.score += collectable.score
  this.hud.updateScoreBoard(this.score)
  this.collectSound.play()
  collectable.disableBody(true, true)
}

function setupCamera(playScene, player) {
  const { width, height, mapOffset } = SHARED_CONFIG;
  playScene.physics.world.setBounds(0, 0, width + mapOffset, height + 200)
  playScene.cameras.main.setBounds(0, 0, width + mapOffset, height).setZoom(SHARED_CONFIG.zoomFactor)
  playScene.cameras.main.startFollow(player)
}

function getCurrentLevel(playScene) {
  return playScene.registry.get('level') || 1
}

function endOfLevel(playScene, end, player) {
  const levelEnd = playScene.physics.add.sprite(end.x, end.y, 'end')
    .setAlpha(0)
    .setSize(5, SHARED_CONFIG.height * 2)
    .setOrigin(0.5, 1)

  const eolOverlap = playScene.physics.add.overlap(player, levelEnd, () => {
    eolOverlap.active = false
    if (playScene.registry.get('level') === SHARED_CONFIG.lastLevel) {
      playScene.scene.start('CreditScene')
      return
    }
    playScene.registry.inc('level', 1)
    playScene.registry.inc('unlocked-levels', 1)
    playScene.scene.restart({ gameStatus: 'LEVEL_COMPLETED' })
  })
}

function createPlayer(playScene, start) {
  return new Player(playScene, start.x, start.y);
}

function createEnemies(playScene, spawnLayer, colliders) {
  const enemies = new Enemies(playScene)
  const enemyTypes = enemies.getTypes()

  spawnLayer.objects.forEach((start, i) => {
    //if (i < 2) { return }
    const enemy = new enemyTypes[start.type](playScene, start.x, start.y)
    enemy.setPlatformsColliders(colliders)
    enemies.add(enemy)
  })

  return enemies
}

function getZones(zones) {
  return {
    start: zones.find(zone => zone.name === 'startZone'),
    end: zones.find(zone => zone.name === 'endZone')
  }
}

function onPlayerAndEnemyCollision(enemy, player) {
  player.takesHit(enemy)
}

function onHit(entity, projectiles) {
  entity.takesHit(projectiles)
}

function addEnemyColliders(enemies, { colliders }) {
  enemies
    .addCollider(colliders.platformsColliders)
    .addCollider(colliders.player, onPlayerAndEnemyCollision)
    .addCollider(colliders.player.projectiles, onHit)
    .addOverlap(colliders.player.meeleWeapon, onHit)
}

function createLayers(playScene, map) {
  const tileset1 = map.getTileset('main_lev_build_1')
  const tileset2 = map.getTileset('main_lev_build_2')
  const tileseteBg = map.getTileset('bg_spikes_tileset')
  const tilesets = [tileset1, tileset2]
  map.createStaticLayer('distance', tileseteBg).setDepth(-3)

  const colliders = map.createStaticLayer('colliders', tileset1)
  const environment = map.createStaticLayer('environment', tileset1).setDepth(-2)
  const platforms = map.createStaticLayer('platforms', tilesets[getCurrentLevel(playScene) - 1])

  const playerZones = map.getObjectLayer('player_zones')
  const enemySpawns = map.getObjectLayer('enemy_spawns')
  const collectables = map.getObjectLayer('collectables')
  const traps = map.createStaticLayer('traps', tileset1)

  colliders.setCollisionByProperty({ collides: true })
  traps.setCollisionByExclusion(-1)

  return {
    environment,
    platforms,
    colliders,
    playerZones,
    enemySpawns,
    collectables,
    traps
  }
}

function createCollectables(playScene, collectableLayer) {
  const collectables = new Collectables(playScene).setDepth(-1);

  collectables.addFromLayer(collectableLayer)
  collectables.playAnimation('diamond-shine')

  return collectables
}

export default PlayScene;
