"use strict"

import Phaser from 'phaser'
import BaseScene from './base_scene'

function CreditScene() {
  BaseScene.call(this, this.constructor.name, { canGoBack: true })

  this.menu = [
    { scene: null, text: 'Thank you for playing' },
    { scene: null, text: 'Author: Boni' },
  ]
}
CreditScene.prototype = Object.create(BaseScene.prototype)
CreditScene.prototype.constructor = CreditScene

CreditScene.prototype.create = function() {
  // call super, eg: Basescene.prototype
  CreditScene.prototype.__proto__.create.call(this)
  this.createMenu(() => { })
}

export default CreditScene
