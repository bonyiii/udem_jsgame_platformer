"use strict"

import Phaser from 'phaser'

const PLAYER_LOOSE = "PLAYER_LOOSE"

function Emitter() {
  Phaser.Events.EventEmitter.call(this)
}
Emitter.prototype = Object.create(Phaser.Events.EventEmitter.prototype)
Emitter.prototype.constructor = Emitter

const eventEmitter = new Emitter()
export default eventEmitter
export { PLAYER_LOOSE }
