"use strict"

import Phaser from 'phaser'
import playerAnimations from './anims/player_animations';
import collidable from '../mixins/collidable'
import anims from '../mixins/anims'
import HealthBar from '../hud/health_bar';
import SHARED_CONFIG from '../config';
import Projectiles from '../attacks/projectiles';
import MeeleWeapon from '../attacks/meele_weapon';
import eventEmitter, { PLAYER_LOOSE } from '../events/Emitter';

function Player(scene, x, y) {
  Phaser.Physics.Arcade.Sprite.call(this, scene, x, y, 'player')
  const self = this
  initScene()
  initPlayer()
  handleAttacks()
  handleMovements()

  scene.time.addEvent({
    delay: 300,
    repeat: -1,
    callbackScope: this,
    callback: () => {
      if (this.isPlayingAnims('run')) {
        this.stepSound.play()
      }
    }
  })

  self.takesHit = function(source) {
    if (self.hasBeenHit) { return }

    self.hasBeenHit = true;
    // source.properties.damage is for hitting tiles like traps
    // define in Tiled map editor
    self.health -= source.damage || source.properties.damage || 0
    if (self.health <= 0) {
      eventEmitter.emit(PLAYER_LOOSE)
      return
    }

    bounceOff(source)
    const hitAnimation = playDamageTween()
    self.healthBar.decrease(self.health)

    source.deliversHit && source.deliversHit(self)

    scene.time.delayedCall(1000, () => {
      self.hasBeenHit = false;
      hitAnimation.stop();
      self.clearTint();
    })
  }

  function playDamageTween() {
    return scene.tweens.add({
      targets: self,
      duration: 120,
      repeat: -1,
      tint: 0xffffff
    })
  }

  function bounceOff(source) {
    if (source.body) {
      // when touching an object like enemy or projectile
      self.body.touching.right ?
        self.setVelocityX(-self.bounceVelocity) :
        self.setVelocityX(self.bounceVelocity)
    } else {
      // when touching a tile, like trap
      self.body.blocked.right ?
        self.setVelocityX(-self.bounceVelocity) :
        self.setVelocityX(self.bounceVelocity)
    }
    setTimeout(() => { self.setVelocityY(-self.bounceVelocity) }, 0)
  }

  function update() {
    if (self.hasBeenHit || self.isSliding || !self.body) { return }

    if (self.getBounds().top > SHARED_CONFIG.height + 100) {
      eventEmitter.emit(PLAYER_LOOSE)
      return
    }

    const { left, right, space, down } = self.cursors;
    const isSpaceJustDown = Phaser.Input.Keyboard.JustDown(space)
    const onFloor = self.body.onFloor();

    if (left.isDown) {
      self.lastDirection = Phaser.Physics.Arcade.FACING_LEFT;
      self.setVelocityX(-self.playerSpeed)
      self.setFlipX(true)
    } else if (right.isDown) {
      self.lastDirection = Phaser.Physics.Arcade.FACING_RIGHT;
      self.setVelocityX(self.playerSpeed)
      self.setFlipX(false)
    } else {
      self.setVelocityX(0)
    }

    if ((isSpaceJustDown) && (onFloor || self.jumpCount < self.consecutiveJumps)) {
      self.jumpSound.play();
      self.setVelocityY(-self.playerSpeed * 2);
      self.jumpCount++;
    }

    if (onFloor) {
      self.jumpCount = 0;
    }

    animate(onFloor)
  }

  function animate(onFloor) {
    if (self.isPlayingAnims('slide')) { return }
    if (self.isPlayingAnims('throw')) { return }

    if (onFloor) {
      if (self.body.velocity.x === 0) {
        self.play('idle', true)
      } else {
        self.play('run', true)
      }
    } else {
      self.play('jump', true)
    }
  }

  function initScene() {
    // The player sprite added to the scene by scene.add.existing
    // so that it will be visible
    scene.add.existing(self)
    scene.physics.add.existing(self);
    scene.events.on(Phaser.Scenes.Events.UPDATE, update)
  }

  function handleAttacks() {
    scene.input.keyboard.on("keydown-Q", () => {
      self.projectileSound.play()
      self.play('throw', true)
      self.projectiles.fireProjectile(self, 'iceball')
    })

    scene.input.keyboard.on("keydown-E", () => {
      self.meeleWeapon.swing(self, self.swipeSound)
    })
  }

  function handleMovements() {
    scene.input.keyboard.on("keydown-DOWN", () => {
      if (self.body.onFloor()) {
        self.body.setSize(self.width, self.height / 2)
        self.setOffset(0, self.height / 2)
        self.setVelocityX(0)
        self.play('slide', true)
        self.isSliding = true
      }
    })

    scene.input.keyboard.on("keyup-DOWN", () => {
      self.body.setSize(self.width, self.height)
      self.setOffset(0, 0)
      self.isSliding = false
    })
  }

  function initPlayer() {
    playerAnimations(scene.anims)

    self.projectileSound = scene.sound.add('projectile-launch', { volume: 0.2 })
    self.stepSound = scene.sound.add('step', { volume: 0.2 })
    self.jumpSound = scene.sound.add('jump', { volume: 0.2 })
    self.swipeSound = scene.sound.add('swipe', { volume: 0.2 })

    // self.body is provided by scene.physics.add.existing
    // that's where the player is added to the scene physics engine
    self.body.setGravityY(500);
    self.body.setSize(20, 32)
    self.setCollideWorldBounds(true);
    // Player sprite bottom middle will be the origin
    self.setOrigin(0.5, 1)
    self.cursors = scene.input.keyboard.createCursorKeys();
    self.lastDirection = Phaser.Physics.Arcade.FACING_RIGHT;
    self.projectiles = new Projectiles(scene, 'iceball-1');
    self.meeleWeapon = new MeeleWeapon(scene, 0, 0, 'sword-default')
    self.playerSpeed = 150;
    self.jumpCount = 0;
    self.consecutiveJumps = 1;
    self.hasBeenHit = false;
    self.bounceVelocity = 250;
    self.health = 100
    self.isSliding = false
    self.healthBar = new HealthBar(
      scene,
      SHARED_CONFIG.leftTopCorner.x + 5,
      SHARED_CONFIG.leftTopCorner.y + 5,
      2,
      self.health)
  }
}
Player.prototype = Object.create(Phaser.Physics.Arcade.Sprite.prototype)
Player.prototype.constructor = Player

// Mixins
Object.assign(Player.prototype, collidable)
Object.assign(Player.prototype, anims)

export default Player;
