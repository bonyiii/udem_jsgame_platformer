"use strict"

import Phaser from 'phaser'
import collidable from '../mixins/collidable'
import birdmanAnimations from './anims/enemies_animations'
import Enemy from './enemy'

function Birdman(scene, x, y) {
  Enemy.call(this, scene, x, y, 'birdman')
  this.className = 'Birdman'

  this.body.setSize(20, 45)
  this.body.setOffset(7, 20)

  birdmanAnimations(scene.anims);
}
Birdman.prototype = Object.create(Enemy.prototype)
Birdman.prototype.constructor = Birdman

Birdman.prototype.takesHit = function(initiator) {
  Birdman.prototype.__proto__.takesHit.call(this, initiator)
  this.play('birdman-hurt', true)
}

Birdman.prototype.update = function(time, delta) {
  // calling Enemy prototype update (which is the prototype object of Birdman.prototype)
  Birdman.prototype.__proto__.update.call(this, time, delta);

  if (!this.active) { return }
  if (this.isPlayingAnims('birdman-hurt')) { return }

  this.play('birdman-idle', true);
}
export default Birdman;
