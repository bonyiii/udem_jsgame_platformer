"use strict"

import Phaser from 'phaser'
import Projectiles from '../attacks/projectiles'
import collidable from '../mixins/collidable'
import snakyAnimations from './anims/snaky_animations'
import Enemy from './enemy'

function Snaky(scene, x, y) {
  Enemy.call(this, scene, x, y, 'snaky')
  this.className = 'Snaky'

  this.speed = 40;
  this.projectiles = new Projectiles(scene, 'fireball-1');
  this.timeFromLastAttack = 0;
  this.attackDelay = getAttackDelay()
  this.lastDirection = null;

  this.body.setSize(this.width - 20, 45)
  this.body.setOffset(10, 15)

  snakyAnimations(scene.anims);
}
Snaky.prototype = Object.create(Enemy.prototype)
Snaky.prototype.constructor = Snaky

Snaky.prototype.takesHit = function(initiator) {
  Snaky.prototype.__proto__.takesHit.call(this, initiator)
  this.play('snaky-hurt', true)
}

Snaky.prototype.update = function(time, delta) {
  // calling Enemy prototype update (which is the prototype object of Snaky.prototype)
  // "this" is the object which is created by the Snaky constructor function
  Snaky.prototype.__proto__.update.call(this, time, delta);

  if (!this.active) { return }
  if (this.body.velocity.x > 0) {
    this.lastDirection = Phaser.Physics.Arcade.FACING_RIGHT
  } else {
    this.lastDirection = Phaser.Physics.Arcade.FACING_LEFT
  }

  if (this.timeFromLastAttack + this.attackDelay <= time) {
    this.projectiles.fireProjectile(this, 'fireball')
    this.timeFromLastAttack = time
    this.attackDelay = getAttackDelay()
  }

  if (!this.active) { return }
  if (this.isPlayingAnims('snaky-hurt')) { return }

  this.play('snaky-walk', true);
}

function getAttackDelay() {
  return Phaser.Math.Between(1000, 4000)
}

export default Snaky
