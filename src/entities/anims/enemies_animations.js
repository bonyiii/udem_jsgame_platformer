import playerAnimations from "./player_animations";

function birdmanAnimations(anims) {
  anims.create({
    key: 'birdman-idle',
    frames: anims.generateFrameNumbers('birdman', { start: 0, end: 8 }),
    frameRate: 8,
    repeat: -1
  })

  anims.create({
    key: 'run',
    frames: anims.generateFrameNumbers('player', { start: 11, end: 16 }),
    frameRate: 8,
    repeat: -1
  })

  anims.create({
    key: 'birdman-hurt',
    frames: anims.generateFrameNumbers('birdman', { start: 25, end: 26 }),
    frameRate: 10,
    repeat: 0
  })
}

export default birdmanAnimations
