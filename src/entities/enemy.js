"use strict"

import Phaser from 'phaser'
import SHARED_CONFIG from '../config'
import anims from '../mixins/anims'
import collidable from '../mixins/collidable'

function Enemy(scene, x, y, kind) {
  Phaser.Physics.Arcade.Sprite.call(this, scene, x, y, kind)

  this.scene = scene
  this.scene.add.existing(this)
  this.scene.physics.add.existing(this);

  this.maxPatrolDistance = 250;
  this.currentPatrolDistance = 0;
  this.timeFromLastTurn = 0;
  this.speed = 75;
  this.damage = 10;
  this.health = 40;
  this.body.setGravityY(500);
  this.setCollideWorldBounds(true);
  this.setOrigin(0.5, 1)
  this.setImmovable(true)
  this.rayGraphics = scene.add.graphics({ lineStyle: { width: 2, color: 0xaa00aa } })
  this.platformsColliders = null;
  this.setVelocityX(this.speed);

  scene.events.on(Phaser.Scenes.Events.UPDATE, this.update, this);
}

// These methods defined only once when the file read by parser
// Enemy constructo function invoked multiple times, whenever a new enemy created
Enemy.prototype = Object.create(Phaser.Physics.Arcade.Sprite.prototype)
Enemy.prototype.constructor = Enemy

// Mixins are applied on the prototype object, this way these methods applied once
Object.assign(Enemy.prototype, collidable)
Object.assign(Enemy.prototype, anims)

Enemy.prototype.update = function(time, delta) {
  if (this.getBounds().bottom > 600) {
    this.scene.events.removeListener(Phaser.Scenes.Events.UPDATE, this.update, this)
    this.setActive(false)
    this.rayGraphics.clear()
    this.destroy()
    return
  }
  if (this.body && this.body.onFloor()) { patrol(this, time) }
}

Enemy.prototype.setPlatformsColliders = function(platformsColliders) {
  this.platformsColliders = platformsColliders
}

Enemy.prototype.takesHit = function(initiator) {
  initiator.deliversHit(this)
  this.health -= initiator.damage

  if (this.health <= 0) {
    this.setTint(0xff0000);
    this.setVelocity(0, -200);
    this.body.checkCollision.none = true
    this.setCollideWorldBounds(false);
  }
}

// Kinda private function not available from anywhere else then this file
function patrol(self, time) {
  self.currentPatrolDistance += Math.abs(self.body.deltaX());

  const { ray, hasHit } = self.raycast(self.body, {
    raylength: 25,
    precision: 1,
    steepnes: 0.2
  })

  if ((!hasHit || self.currentPatrolDistance >= self.maxPatrolDistance)
    && self.timeFromLastTurn + 100 < time) {
    /*
     *       console.log(
     *         'hashit', hasHit, 'dist', self.currentPatrolDistance >= self.maxPatrolDistance,
     *         'turntime', self.timeFromLastTurn + 100 < time, time) */

    self.timeFromLastTurn = time
    self.setFlipX(!self.flipX)
    self.setVelocityX(self.speed = -self.speed)
    self.currentPatrolDistance = 0
  }

  if (SHARED_CONFIG.debug && ray) {
    self.rayGraphics.clear()
    self.rayGraphics.strokeLineShape(ray)
  }
}

export default Enemy
